<?php

//[SECTION] Objects as Variables

$buildingObj = (object)[
    'name' => 'Caswynn Building',
    'floors' => 8,
    'address' => (object)[
        'barangay' => 'Sacred Heart',
        'city' => 'Quezon City',
        'country' => 'Philippines'
    ]
];

//[SECTION] Objects from Classes

//Classes serve as blueprints from which objects are created. Objects created from the Building class below MUST contain the specific properties listed in the class (name, floors, address), and no exceptions or mistakes are allowed.

//This gives our object data a consistent template or format to follow

//By convention, class names are capitalized
class Building
{
    public $name;
    public $floors;
    public $address;

    //A constructor is the function that is called when creating an object from the class and serves to set the object's specific values

    //Unlike most functions, the constructor has a pre-set name (__construct) and cannot be named anything else
    public function __construct($name, $floors, $address)
    {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    //Class methods
    public function printName()
    {
        return "The name of the building is $this->name";
    }
}

//[SECTION] Inheritance and Polymorphism

class Condominium extends Building
{
    public function printName()
    {
        return "The name of the condominium is $this->name";
    }

    public function printFloors()
    {
        return $this->floors;
    }
}

//Object creation from a class
$building = new Building('Caswynn Building', 8, 'Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, "Makati City, Philippines");
