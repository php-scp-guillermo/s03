<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S3: Classes and Objects</title>
</head>

<body>
    <h1>Objects from Classes</h1>
    <?php var_dump($building); ?>

    <p><?= $building->printName(); ?></p>

    <h1>Inheritance and Polymorphism</h1>

    <?php var_dump($condominium); ?>

    <p><?= $condominium->printName(); ?></p>
    <?php var_dump(new Building('s', 's', 's')) ?>
</body>

</html>