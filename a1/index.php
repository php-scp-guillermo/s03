<?php
require_once './code.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        div {
            width: 600px;
            margin: 3rem auto;
            border: 5px solid black;
            padding: 1rem;
        }
    </style>
</head>

<body>
    <div>
        <h1>Person</h1>
        <p>
            <?php
            $person = new Person('Rodrigo', 'Roa', 'Duterte');
            echo $person->printName();
            ?>
        </p>

        <h1>Developer</h1>
        <p>
            <?php
            $developer = new Developer('Gabriel', 'Delarmente', 'Guillermo');
            echo $developer->printName();
            ?>
        </p>
        <h1>Engineer</h1>
        <p>
            <?php
            $engineer = new Engineer('Stephen', 'William', 'Hawking');
            echo $engineer->printName();
            ?>
        </p>
    </div>
</body>

</html>