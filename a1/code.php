<?php
class Person
{
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName)
    {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName()
    {
        return "Your Full Name is $this->firstName $this->middleName $this->lastName";
    }
}

class Engineer extends Person
{
    public function printName()
    {
        return "You are an Engineer named $this->firstName $this->middleName $this->lastName";
    }
}

class Developer extends Person
{
    public function printName()
    {
        return "Your name is $this->firstName $this->middleName $this->lastName and you are Developer";
    }
}
